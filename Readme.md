Flickr Project - Its an application utilizing flickr APIs to display photos.

Description - It display list of all recent photos from flickr and allows user to search based on the text . Also , when clicked on any image , it displays enlarge photo with photo detail .

Library Used -

   1)RecyclerView : A flexible view for providing a limited window into a large data set.In this case , re-using views while scrolling to display photos. It is a good practice to use recylerview instead of listview . Its more efficient in terms of memory utilization .
   2)Glide Image library : Glide’s primary focus is on making scrolling any kind of a list of images as smooth and fast as possible, but Glide is also effective for almost any case where you need to fetch, resize, and display a remote image.
   3)Network connection (Custom library) : It is custom library to make any kind of network calls.

Instructions : Download project from GitLab url and import into Android studio .

Copyright © Jyoti Gupta jyotigupta275@gmail.com
