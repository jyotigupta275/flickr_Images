package adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.flickrimages.android.flickr_images.Detail;
import com.flickrimages.android.flickr_images.R;
import models.PhotoModel;

import java.util.ArrayList;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.ViewHolder> {

    private ArrayList<PhotoModel> dataList = new ArrayList<>();
    private Context context;

    public PhotosAdapter(ArrayList<PhotoModel> data, Context c){
        dataList = data;
        context = c;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_photo,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Glide.with(context).load(dataList.get(position).getPathMediumImage()).into(holder.picture);
        holder.picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Detail.class);
                intent.putExtra("image",dataList.get(position).getPathLargeImage());
                intent.putExtra("text",dataList.get(position).getTitle());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        final private ImageView picture;

        public ViewHolder(View v){
            super(v);
            picture = (ImageView)v.findViewById(R.id.picture);
        }
    }
}
