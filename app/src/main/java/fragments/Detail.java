package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flickrimages.android.flickr_images.R;


public class Detail extends Fragment{

    private String title;
    private String urlImage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title = getActivity().getIntent().getStringExtra("text");
        urlImage = getActivity().getIntent().getStringExtra("image");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail,container,false);

        TextView titleText = (TextView)view.findViewById(R.id.textDetail);
        titleText.setText(title);
        if (title.length()<1){
            titleText.setVisibility(View.GONE);
        }

        ImageView image = (ImageView)view.findViewById(R.id.imageDetail);
        Glide.with(getContext()).load(urlImage).into(image);

        return view;
    }
}
