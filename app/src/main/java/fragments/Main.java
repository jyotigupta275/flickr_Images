package fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import adapters.PhotosAdapter;
import com.flickrimages.android.flickr_images.R;
import json.JsonParser;
import models.PhotoModel;
import tasks.NetworkConnection;
import tasks.PhotoTask;
import utilities.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main extends Fragment{

    private RecyclerView list;
    private PhotosAdapter adapter;
    private ArrayList<PhotoModel> data;
    private String search;
    private ProgressDialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        data = new ArrayList<>();
        dialog = new ProgressDialog(getContext());
        dialog.setIndeterminate(true);
        dialog.setMessage(getString(R.string.refreshing));
        dialog.setCanceledOnTouchOutside(false);

        refreshPictures();

        list = (RecyclerView)view.findViewById(R.id.listPhotos);
        adapter = new PhotosAdapter(data, getContext());
        list.setAdapter(adapter);
        list.setLayoutManager(new GridLayoutManager(getContext(),3));
        list.setHasFixedSize(true);

        final EditText searchText = (EditText)view.findViewById(R.id.searchEditText);
        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                boolean action = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Utils.hideKeyboard(getActivity());
                    search = searchText.getText().toString();
                    if (search.length() > 0){
                        refreshSearch(search);
                    }
                }
                return action;
            }
        });

        ImageView serachImage = (ImageView)view.findViewById(R.id.searchImage);
        serachImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hideKeyboard(getActivity());
                search = searchText.getText().toString();
                if (search.length() > 0){
                    refreshSearch(search);
                }
            }
        });

        return view;
    }

    private void refreshSearch(String search) {
        if (!dialog.isShowing()){
            dialog.show();
        }
        Uri uri = Uri.parse("rest");
        Map<String, String> params = new HashMap<>();
        params.put("method",getString(R.string.parameter_method_search));
        params.put("api_key",getString(R.string.app_key));
        params.put("text",search);
        params.put("format","json");
        params.put("nojsoncallback","1");
        NetworkConnection.with(getContext()).withListener(new NetworkConnection.ResponseListener() {
            @Override
            public void onSuccessfullyResponse(String response) {
                data = JsonParser.getPhotoIds(response);
                if(data.isEmpty()){
                    if (dialog.isShowing()){
                        dialog.dismiss();
                    }
                    CharSequence text = "No results found";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(getContext(), text, duration);
                    toast.show();
                }else{
                    getPaths(0);
                }
            }

            @Override
            public void onErrorResponse(String error, String message, int code) {

            }
        }).doRequest(PhotoTask.REQUEST.GET,uri,params,null,null);
    }

    private void refreshPictures() {
        if (!dialog.isShowing()){
            dialog.show();
        }
        Uri uri = Uri.parse("rest");
        Map<String, String> params = new HashMap<>();
        params.put("method",getString(R.string.parameter_method_recent));
        params.put("api_key",getString(R.string.app_key));
        params.put("format","json");
        params.put("nojsoncallback","1");
        NetworkConnection.with(getContext()).withListener(new NetworkConnection.ResponseListener() {
            @Override
            public void onSuccessfullyResponse(String response) {
                data = JsonParser.getPhotoIds(response);
                if(data.isEmpty()){
                    if (dialog.isShowing()){
                        dialog.dismiss();
                    }
                    CharSequence text = "No results found";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(getContext(), text, duration);
                    toast.show();
                }else{
                    getPaths(0);
                }
            }

            @Override
            public void onErrorResponse(String error, String message, int code) {

            }
        }).doRequest(PhotoTask.REQUEST.GET,uri,params,null,null);
    }

    private void getPaths(final int current) {
        Uri uri = Uri.parse("rest");
        Map<String, String> params = new HashMap<>();
        params.put("method",getString(R.string.parameter_method_get_sizes));
        params.put("api_key",getString(R.string.app_key));
        params.put("photo_id", data.get(current).getPhotoId());
        params.put("format","json");
        params.put("nojsoncallback","1");
        NetworkConnection.with(getContext()).withListener(new NetworkConnection.ResponseListener() {
            @Override
            public void onSuccessfullyResponse(String response) {
                PhotoModel model = JsonParser.getUrlImages(response,data.get(current));
                data.set(current,model);
                int newCurrent = current + 1;
                if (newCurrent < data.size()) {
                    getPaths(newCurrent);
                }else {
                    refreshUI();
                }
            }

            @Override
            public void onErrorResponse(String error, String message, int code) {

            }
        }).doRequest(PhotoTask.REQUEST.GET,uri,params,null,null);
    }

    private void refreshUI() {
        adapter = new PhotosAdapter(data, getContext());
        list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        if (dialog.isShowing()){
            dialog.dismiss();
        }
    }
}
