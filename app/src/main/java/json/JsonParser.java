package json;


import models.PhotoModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JsonParser {

    public static ArrayList<PhotoModel> getPhotoIds(String JSONStr){
        ArrayList<PhotoModel> photoModels = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(JSONStr);
            JSONObject obj = object.getJSONObject("photos");
            JSONArray listPhotos = obj.getJSONArray("photo");
            for (int i = 0 ; i < listPhotos.length() ; i++){
                JSONObject phId = listPhotos.getJSONObject(i);
                String id = phId.optString("id","");
                String title = phId.optString("title","");
                PhotoModel model = new PhotoModel(id,title);
                photoModels.add(model);
            }
            return photoModels;
        }catch (JSONException e){
            e.printStackTrace();
            return photoModels;
        }
    }

    public static PhotoModel getUrlImages(String JSONStr, PhotoModel model){
        PhotoModel photoModel = model;
        try {
            JSONObject object = new JSONObject(JSONStr);
            JSONObject objectSizes = object.getJSONObject("sizes");
            JSONArray sizes = objectSizes.getJSONArray("size");
            for (int i = 0 ; i < sizes.length() ; i++){
                JSONObject obj = sizes.getJSONObject(i);
                String label = obj.getString("label");
                if (label.toLowerCase().equalsIgnoreCase("small")){
                    String source = obj.optString("source","");
                    photoModel.setPathMediumImage(source);
                }
                if (label.toLowerCase().equalsIgnoreCase("large")){
                    String source = obj.optString("source","");
                    photoModel.setPathLargeImage(source);
                }
            }
            return photoModel;
        }catch (JSONException e){
            e.printStackTrace();
            return photoModel;
        }
    }

}
