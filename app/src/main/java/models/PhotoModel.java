package models;

public class PhotoModel {
    private String photoId;
    private String title;
    private String pathMediumImage;
    private String pathLargeImage;

    public PhotoModel(String photoId, String title) {
        this.photoId = photoId;
        this.title = title;
        this.pathMediumImage = "";
        this.pathLargeImage = "";
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getPathMediumImage() {
        return pathMediumImage;
    }

    public void setPathMediumImage(String path) {
        this.pathMediumImage = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPathLargeImage() {
        return pathLargeImage;
    }

    public void setPathLargeImage(String pathLargeImage) {
        this.pathLargeImage = pathLargeImage;
    }
}
