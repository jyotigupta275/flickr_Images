package tasks;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;

import org.json.JSONObject;

import java.util.Map;

public class NetworkConnection implements PhotoTask.ConnectionListener{

    private Context context;
    private static NetworkConnection INSTANCE = null;
    private ResponseListener listener;

    private static NetworkConnection getInstance(){
        if (INSTANCE == null){
            createInstance();
        }
        return INSTANCE;
    }

    private static synchronized void createInstance(){
        if (INSTANCE == null){
            INSTANCE = new NetworkConnection();
        }
    }

    public NetworkConnection(){}

    public static NetworkConnection with(Context context){
        getInstance().setContext(context);
        return getInstance();
    }

    public NetworkConnection withListener(ResponseListener listener){
        getInstance().setListener(listener);
        return getInstance();
    }

    public void doRequest(PhotoTask.REQUEST method, final Uri uri, @Nullable final Map<String, String> params, @Nullable final Map<String, String> headers, @Nullable JSONObject jsonObject){
        PhotoTask connection = new PhotoTask(context,uri.toString(),method,params,headers,jsonObject);
        connection.setListener(this);
        connection.execute();
    }


    public void setContext(Context context) {
        this.context = context;
    }

    public void setListener(ResponseListener listener) {
        this.listener = listener;
    }

    @Override
    public void successfullyResponse(String JSONStr) {
        listener.onSuccessfullyResponse(JSONStr);
    }

    @Override
    public void errorResponse(String error, String message, int codeError) {
        listener.onErrorResponse(error,message,codeError);
    }

    public interface ResponseListener{
        void onSuccessfullyResponse(String response);
        void onErrorResponse(String error, String message, int code);
    }
}
